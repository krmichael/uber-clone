import React from 'react';
import { StatusBar } from 'react-native';

import Map from './components/Map';

export default function App() {
  return (
    <>
      <StatusBar backgroundColor="transparent" barStyle="dark-content" />
      <Map />
    </>
  );
}
