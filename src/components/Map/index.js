import React from 'react';
import { View, StyleSheet } from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import MapView from 'react-native-maps';

import Search from '../Search';
import Directions from '../Directions';
import { getPixelSize } from '../../../utils';

export default class Map extends React.Component {
  state = {
    region: null,
    destination: null,
  };

  componentDidMount() {
    Geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        this.setState({
          region: {
            latitude,
            longitude,
            latitudeDelta: 0.0143,
            longitudeDelta: 0.0134,
          },
        });
      },
      () => {},
      {
        timeout: 2000,
        enableHighAccuracy: true,
        maximumAge: 1000,
      },
    );
  }

  handleLocationSelected = (data, { geometry }) => {
    const {
      location: { lat: latitude, lng: longitude },
    } = geometry;

    this.setState({
      destination: {
        latitude,
        longitude,
        title: data.structured_formatting.main_text,
      },
    });
  };

  render() {
    const { container } = styles;
    const { region, destination } = this.state;

    return (
      <View style={container}>
        <MapView
          style={container}
          region={region}
          showsUserLocation
          loadingEnabled
          ref={elem => (this.mapView = elem)}>
          {destination && (
            <Directions
              origin={region}
              destination={destination}
              onReady={result => {
                this.mapView.fitToCoordinates(result.coordinates, {
                  edgePadding: {
                    right: getPixelSize(50),
                    left: getPixelSize(50),
                    top: getPixelSize(50),
                    bottom: getPixelSize(50),
                  },
                });
              }}
            />
          )}
        </MapView>

        <Search onLocationSelected={this.handleLocationSelected} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
